<?php

class PEXAPI {

	var $apikey = '';
	var $apisecret = '';
	var $prod_endpoint = 'https://coreapi.pexcard.com/v4/';
	var $dev_endpoint = 'https://sandbox-coreapi.pexcard.com/v4/';
	var $endpoint = '';

	function __construct( $key = '', $secret = '', $sandbox = false ) {
		$this->apikey = $key;
		$this->apisecret = $secret;
		$this->endpoint = $this->dev_endpoint;
		if ( ! $sandbox ) $this->endpoint = $this->prod_endpoint;
	}

	function get_token() {
		$tokens = $this->get_tokens();
		$token = '';
		if ( $tokens ) {
			foreach ( $tokens as $t ) {
				if ( $t['SecondsUntilExpire'] > 0 ) {
					$token = $t['Token'];
					break;
				}
			}
		}
		if ( ! $token ) $token = $this->create_token();
		return $token;
	}

	function get_tokens() {
		$result = $this->call( "/Token/GetAuthTokens" );
		$tokens = json_decode( $result, true );
		return $tokens;
	}

	function create_token( $user = '', $pass = '' ) {
		$result = $this->call( 'Token', array( 'Username' => $user, 'Password' => $pass ) );
		$result = json_decode( $result, true );
		if ( isset( $result['Token'] ) ) {
			return $result['Token'];
		}
		return;
	}

	function get_accounts( $token = '' ) {
		if ( $token ) {
			$result = $this->call( "/Details/AccountDetails", '', $token );
			$accounts = json_decode( $result, true );
			if ( isset( $accounts['CHAccountList'] ) ) return $accounts['CHAccountList'];
		}
		return;
	}

	function get_account( $account_id = 0, $token = '' ) {
		if ( $account_id > 0 && $token ) {
			$result = $this->call( "/Details/AccountDetails/$account_id", '', $token );
			$account = json_decode( $result, true );
			return $account;
		}
		return;
	}

	function call( $function = '', $data = array(), $token = '', $verbose = false ) {
		$url = $this->endpoint . $function;
		$fields = array();
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 ); 
		if ( $data ) {
			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );
		}
		if ( $token ) $auth = 'token ' . $token;
		else $auth = 'basic ' . base64_encode( $this->apikey . ':' . $this->apisecret );
		$headers = [ "Content-Type: application/json", "Accept: application/json", "Authorization: $auth" ];
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		if ( $verbose ) curl_setopt( $ch, CURLOPT_VERBOSE, true );
		$result = curl_exec( $ch );
		curl_close( $ch );
		return $result;
	}
}

?>
