# PEX API
* * *

PEX is an Employee Expense Management & Prepaid Card Platform (https://www.pexcard.com/). PEX API documentation can be found here: https://developer.pexcard.com/docs4

PEXAPI.php is a PHP Class to interface with the PEX API. This class currently only pulls data from the API: accounts and balances.

* * *
**HOW TO USE?**
* * *

Instantiate by passing in API key (required), API secret (required), and boolean flag for sandbox endpoint (optional -- default false):

	require_once 'PEXAPI.php';
	$pex = new PEXAPI( APIKEY, APISECRET, true|false );

In order to use the API functions, you first need to generate an authentication token. To do this, use the get_token method and pass in your API username and password:

	$token = $pex->get_token( APIUSER, APIPASS );

The PEXAPI class currently offers the ability to use the get_accounts (/Details/AccountDetails) and get_account (/Details/AccountDetails/{Id}) methods. Below is a code block for displaying associated accounts:

	if ( $token ) {
		$accounts = $pex->get_accounts( $token );
		echo '<hr><br>';
		foreach ( $accounts as $a ) {
			$account = $pex->get_account( $a['AccountId'], $token );
			echo "Account ID: " . $account['AccountId'] . "<br>";
			echo "Email: " . $account['Email'] . "<br>";
			echo "Account Status: " . $account['AccountStatus'] . "<br>";
			echo "Ledger Balance: " . $account['LedgerBalance'] . "<br>";
			echo "Available Balance: " . $account['AvailableBalance'] . "<br>";
			if ( isset( $account['CardList'] ) ) {
				echo '<h4>Cards</h4>';
				echo "<ul>";
				foreach ( $account['CardList'] as $c ) {
					echo '<li>Last 4: ' . $c['Last4CardNumber'] . ', Issued: ' . $c['IssuedDate'] . ', Exp: ' . $c['ExpirationDate'] . '</li>';
				}
				echo "</ul>";
			}
		}
		echo '<br><hr>';
	}

* * *
**NOTE: it is important to provide Rate Limiting in your code. We suggest caching the results of your API function calls to a minimum of one minute. If using WordPress, we suggest using transient data functions, like this.**
* * *

	$token = get_transient( 'PEXAPI-TOKEN' );
	if ( ! $token ) $token = $pex->get_token( APIUSER, APIPASS );
	set_transient( 'PEXAPI-TOKEN', $token, 60 );

	if ( $token ) {
		$accounts = get_transient( 'PEXAPI-ACCOUNTS' );
		if ( ! $accounts ) $accounts = $pex->get_accounts( $token );
		set_transient( 'PEXAPI-ACCOUNTS', $accounts, 60 );
		...
	}
